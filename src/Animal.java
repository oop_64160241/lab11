public abstract class Animal {
    private String name;
    private int numberOfleg;
    public Animal(String name, int numberOfleg) {
        this.name = name;
        this.numberOfleg = numberOfleg;
    }
    public String getName() {
        return name;
    }
    public int getNumberOfleg() {
        return numberOfleg;
    }
    public void setName(String name) {
        this.numberOfleg = numberOfleg;
    }
    @Override
    public String toString() {
        return "Animal (" + name + ")has " + numberOfleg + " legs";
    }

    public abstract void eat();
    public abstract void sleep();
}
