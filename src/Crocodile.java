public class Crocodile extends Animal implements Swimable, Crawlable{

    public Crocodile(String name, int numberOfleg) {
        super(name, 4);
    }

    @Override
    public void craw() {
        System.out.println(this + " craw.");
    }

    @Override
    public void swim() {
        System.out.println(this + " swim.");
    }

    @Override
    public void eat() {
        System.out.println(this + " eat.");
    }

    @Override
    public void sleep() {
        System.out.println(this + " sleep.");
    }

    @Override
    public String toString() {
    return "Crocodile("+this.getName()+")";
    }
}
