public class App {
    public static void main(String[] args)  {
       Bat bat1 = new Bat("Batman");
       bat1.eat();
       bat1.sleep();
       bat1.fly();
       bat1.takeoff();
       bat1.landing();

       Fish fish1 = new Fish("Goldfish");
       fish1.eat();
       fish1.sleep();
       Plane plane1 = new Plane("Wind", "Wind Engine");
       plane1.takeoff();
       plane1.fly();
       plane1.landing();

       Crocodile crocodile1 = new Crocodile("Charawan", 0);
       crocodile1.craw();
       crocodile1.eat();
       crocodile1.sleep();
       crocodile1.swim();

       Flyable[] flyablesObjects = {bat1, plane1};
       for(int i=0; i<flyablesObjects.length;i++) {
           flyablesObjects[i].takeoff();
           flyablesObjects[i].fly();
           flyablesObjects[i].landing();
       }
    }
}
