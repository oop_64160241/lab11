public class Fish extends Animal {
    

    public Fish(String name) {
        super(name, 0);
        
    }

    @Override
    public void eat() {
        System.out.println(this.toString() + " eat.");      
    }

    @Override
    public void sleep() {
        System.out.println(this.toString() + " sleep.");
    }
    
    @Override
    public String toString() {
        return "Fish("+this.getName()+")";
    }
}
